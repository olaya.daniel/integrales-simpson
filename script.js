function simpson(nfuncion, b, a)
{
   
    var n = parseInt(prompt("Digite la el número de intervalos", "10"));

    var dn = (b-a)/n;
    var x_0 = a;
    var suma_f = 0;
    var resultado = 0;
  
    var f_eval;

    
    switch(nfuncion){
        case 1:
            f_eval = f1;
            break;
        case 2:
            f_eval = f2;
            break;
        case 3:
            f_eval = f3;
            break;
        case 4:
            f_eval = f4;
            break;
        case 5:
            f_eval = f5;
            break;
        default:
            break;
    }

    suma_f = f_eval(a);

    for(var i=1; i<n; i++){
        x_0 = x_0 + dn;
        if(i%2==0){
            suma_f = suma_f + 2*f_eval(x_0);
        } else if(i%2==1){
            suma_f = suma_f + 4*f_eval(x_0);
        }
    }

    suma_f = suma_f + f_eval(b);

    resultado = (dn/3)*suma_f;

    document.write(resultado);

}

function f1(x){
    
    return (((Math.pow(x,3))/9)*(Math.sqrt(1+((Math.pow(x,4))/9))));

}


function f2(x){

    //REEMPLAZAR POR LA SEGUNDA FUNCIÓN
    //HACER LO MISMO PARA TODAS LAS FUNCIONES
    //return (Math.sin(x)/x);
    return Math.sin(x)/x;

}

function f3(x){

    return (Math.pow(Math.sin(x), 2));

}

function f4(x){

    //REEMPLAZAR POR LA SEGUNDA FUNCIÓN
    //HACER LO MISMO PARA TODAS LAS FUNCIONES
    return (2/(Math.sqrt(Math.PI)))*(Math.exp(-1*Math.pow(x,2)));

}

function f5(x){


    return (Math.sqrt(1+Math.pow(x, 4)));

}